﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpController : MonoBehaviour
{

    [SerializeField]
    GameObject target;


    private void OnTriggerEnter2D(Collider2D collision)
    {
                if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Warp");
            collision.gameObject.transform.position = target.transform.position;
        }
    }
}
