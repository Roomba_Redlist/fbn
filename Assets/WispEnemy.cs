﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WispEnemy : MonoBehaviour
{
    private Vector2 vec;
    private bool isRunning;
    private float pivot;
    public GameObject player;
    public GameObject bullet;
    // Start is called before the first frame update
    void Start()
    {
        pivot = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        vec = (transform.position - player.transform.position).normalized;

        StartCoroutine("Shot");

        Vector3 scale = transform.localScale;
        if (vec.x > 0)
        {
            scale.x = 1;
        }
        else if (vec.x < 0)
        {
            scale.x = -1;
        }
        transform.localScale = scale;
        float sin = Mathf.Sin(Time.time);
        transform.position = new Vector2(transform.position.x, pivot+sin);
    }
    IEnumerator Shot()
    {
        if (isRunning)
        {
            yield break;
        }
        isRunning = true;
        GameObject shot = Instantiate(bullet, transform.position, Quaternion.FromToRotation(Vector3.up, vec));
        yield return new WaitForSeconds(2);
        isRunning = false;
    }
}
