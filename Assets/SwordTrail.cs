﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTrail : MonoBehaviour
{
    public float lifeTime;
    public float Power;
    private int dir;
    private GameObject parent;
    private Rigidbody2D rb;
    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, lifeTime);
        dir = gameObject.GetComponentInParent<PlayerSword>().dir;
        Vector3 scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = dir*scale.x;
        }
        else if (dir < 0)
        {
            scale.x = -dir*scale.x;
        }
        transform.localScale = scale;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy" || collision.tag == "Ground")
        {
            Debug.Log("Entered");
            Debug.Log("Col name=" + collision.name);
            collision.GetComponent<EnemyManager>().Damaged(Power);
        }

    }
}
