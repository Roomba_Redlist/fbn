﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingBullet : MonoBehaviour
{
    public float timeRag = 2f;
    public float bulletSpeed;
    private Vector2 vec;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        timeRag -= Time.deltaTime;
        if (timeRag <= 0)
        {
            vec = (transform.position - player.transform.position).normalized;
            GetComponent<Rigidbody2D>().velocity = vec * bulletSpeed;
        }
    }
}
