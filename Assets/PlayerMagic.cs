﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMagic : MonoBehaviour
{
    public GameObject bullet;
    public int dir;
    private float coolTime = 0;
    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetButton("Fire1") && coolTime == 0)
        {
            dir = gameObject.GetComponentInParent<PlayerController>().playerDirection;
            Debug.Log("playerMagic.dir=" + dir);
            GameObject shot = Instantiate(bullet, transform.position, Quaternion.identity, transform);
            coolTime = 0.3f;
        }
        if (coolTime >= 0)
        {
            coolTime -= Time.deltaTime;
        }
        else if (coolTime <= 0)
        {
            coolTime = 0;
        }
    }
}
