﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : MonoBehaviour
{
    public float Damage;
    private 
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Enemy" && !GetComponentInParent<PlayerController>().invincible)
        {
            Damage = collision.gameObject.GetComponent<HitToPlayer>().enemyPower;
            GetComponentInParent<PlayerController>().Damaged(Damage);
        }
    }
}
