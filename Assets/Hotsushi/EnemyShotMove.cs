﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotMove : MonoBehaviour
{
    private int dir;

    private Rigidbody2D rb;

    // スクリプトが有効になったとき一回だけ呼ばれます
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        dir = gameObject.GetComponentInParent<kumo>().dir;

        rb.velocity = new Vector2(5*dir,0);

    }
}
