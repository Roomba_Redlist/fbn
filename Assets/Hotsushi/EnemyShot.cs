﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    public GameObject Tama; // 弾のオブジェクト

    public static int count; // 発射間隔

    int frame; // フレーム

    public GameObject objA;
    public GameObject objB;

    // Start is called before the first frame update
    void Start()
    {
        frame = 0;
        count = 90;
    }

    // Update is called once per frame
    void Update()
    {
        frame++; // フレームをカウント
        Vector3 Apos = objA.transform.position;
        Vector3 Bpos = objB.transform.position;
        float dis = Vector3.Distance(Apos, Bpos);
        if (dis < 9 && frame % count == 0)
        {
            Instantiate(Tama, new Vector2(transform.position.x, transform.position.y),
                Quaternion.identity); // プレイヤーの位置に弾を生成します
        }
    }
}
