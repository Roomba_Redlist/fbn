﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class suraimu : MonoBehaviour
{
    private int dir = 1;
    private Rigidbody2D rb;
    public GameObject objA;
    public GameObject objB;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        dir = GetComponentInChildren<Inversion>().dir;

        rb.velocity = new Vector2(1 * dir, 0);
        Vector3 scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = -1;
        }
        else if (dir < 0)
        {
            scale.x = 1;
        }
        transform.localScale = scale;

        Vector3 Apos = objA.transform.position;
        Vector3 Bpos = objB.transform.position;
        float dis = Vector3.Distance(Apos, Bpos);
        if (dis < 2)
        {
            rb.velocity = new Vector2(5*dir, 0);
        }
    }
}
