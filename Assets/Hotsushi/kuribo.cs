﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kuribo : MonoBehaviour
{
    private int dir = 1;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        dir = GetComponentInChildren<Inversion>().dir;

        rb.velocity = new Vector2(1*dir, rb.velocity.y);
        Vector3 scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = -1;
        }
        else if (dir < 0)
        {
            scale.x = 1;
        }
        transform.localScale = scale;
    }
}
