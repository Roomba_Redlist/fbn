﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kumo : MonoBehaviour
{
    public int dir = 1;
    private bool isJumping;
    private Rigidbody2D rb;
    public GameObject objA;
    public GameObject objB;

    public static int count;
    int frame;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        frame = 0;
        count = 90;
    }

    // Update is called once per frame
    void Update()
    {
        dir = GetComponentInChildren<Inversion>().dir;

        rb.velocity = new Vector2(1*dir, rb.velocity.y);
        Vector3 scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = -1;
        }
        else if (dir < 0)
        {
            scale.x = 1;
        }
        transform.localScale = scale;

        Vector3 Apos = objA.transform.position;
        Vector3 Bpos = objB.transform.position;
        float dis = Vector3.Distance(Apos, Bpos);

        frame++;
        if (frame % count == 0)
        {
            if (dis < 4 && !isJumping)
            {
                rb.velocity = new Vector2(rb.velocity.x, 10);
                isJumping = true;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        { 
        isJumping = false;
        }
        if (collision.tag == "Enemy")
        {
            isJumping = false;
        }
    }
}
