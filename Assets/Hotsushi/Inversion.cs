﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inversion : MonoBehaviour
{
    public int dir = -1;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            dir = dir * -1;
        }
        if (collision.tag == "Enemy")
        {
            dir = dir * -1;
        }
    }
}