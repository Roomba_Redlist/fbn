﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBullet : MonoBehaviour
{
    public float lifeTime;
    public float bulletSpeed;
    public float Power;
    private int dir;
    private GameObject parent;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, lifeTime);
        dir = gameObject.GetComponentInParent<PlayerGun>().dir;
        Vector3 scale = transform.localScale;
        if (dir > 0)
        {
            scale.x = 1;
        }
        else if (dir < 0)
        {
            scale.x = -1;
        }
        transform.localScale = scale;
    }
    void Update()
    {
        rb.velocity =new Vector2(dir*bulletSpeed,0);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Enemy" || collision.tag == "Ground")
        {
            if (collision.tag == "Enemy")
            {
                Debug.Log("Enemy entered");
                collision.GetComponent<EnemyManager>().Damaged(Power);
            }
            Destroy(gameObject);
        }
        Debug.Log("Entered");

    }
}
