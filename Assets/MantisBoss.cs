﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MantisBoss : MonoBehaviour
{
    private bool isRunning;
    private bool canMove;
    private GameObject player;
    private GameObject slash1;
    private GameObject slash2;
    private float dis;
    private Vector2 vec;
    private int dir;
    private float speed = 4;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
        slash1 = (GameObject)Resources.Load("MantisSlash1");
        slash2 = (GameObject)Resources.Load("MantisSlash2");
        Debug.Log(slash1.tag);
        StartCoroutine("Action");

    }

    // Update is called once per frame
    void Update()
    {
        dis = Vector3.Distance(transform.position, player.transform.position);
        vec = (transform.position - player.transform.position).normalized;
        if (vec.x > 0)
        {
            dir = -1;
        }
        else if (vec.x < 0)
        {
            dir = 1;
        }
    }
    IEnumerator Action()
    {
        while (true)
        {
            canMove = false;
            yield return Move(6);
            yield return new WaitForSeconds(0.3f);
            yield return Charge();
            yield return new WaitForSeconds(4f);
            yield return Move(12);
            yield return new WaitForSeconds(0.4f);
            yield return Slash();
            yield return new WaitForSeconds(2f);
            canMove = true;
        }

    }
    IEnumerator Move(int count)
    {
        while (true)
        {
            Vector3 scale = transform.localScale;
            if (dir > 0)
            {
                scale.x = -1;
            }
            else if (dir < 0)
            {
                scale.x = 1;
            }
            transform.localScale = scale;
            rb.velocity = new Vector2(speed * dir, 0);
            yield return new WaitForSeconds(0.2f);
            rb.velocity = new Vector2(0, 0);
            yield return new WaitForSeconds(0.1f);
            count--;
            if (dis <= 5 || count==0)
            {
                Debug.Log("move end");
                yield break;
            }
        }
    }
    IEnumerator Charge()
    {
        rb.velocity = new Vector2(-5 * dir, 0);
        yield return new WaitForSeconds(0.3f);
        rb.velocity = -vec*20;
        yield return new WaitForSeconds(1.5f);
        rb.velocity = new Vector2(0, 0);
        Debug.Log("charge end");
    }
    IEnumerator Slash()
    {
        GameObject slashone = Instantiate(slash1, new Vector3(transform.position.x + (2 * dir), transform.position.y + 2, 0), Quaternion.identity, transform);
        yield return new WaitForSeconds(0.4f);
        GameObject slashtwo = Instantiate(slash2, new Vector3(transform.position.x + (3 * dir), transform.position.y, 0), Quaternion.identity, transform);
        yield return new WaitForSeconds(0.4f);
        Debug.Log("slash end");
    }
}
