﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSword : MonoBehaviour
{
    public GameObject bullet;
    public int dir;
    private float coolTime=0;
    // Update is called once per frame
    void Update()
    {
        dir = gameObject.GetComponentInParent<PlayerController>().playerDirection;
        if (Input.GetButton("Fire1") && coolTime == 0)
        {
            Debug.Log(coolTime);
            GameObject shot = Instantiate(bullet, new Vector3(transform.position.x+2*dir, transform.position.y), Quaternion.identity);
            shot.transform.parent = this.transform;
            coolTime = 0.6f;
        }
        if (coolTime >= 0)
        {
            coolTime -= Time.deltaTime;
        }else if (coolTime <= 0)
        {
            coolTime = 0;
        }
    }
}
