﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WispBullet : MonoBehaviour
{

    private GameObject player;
    private float deltaAngle;
    private Vector2 vec;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        player=GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        vec = (transform.position - player.transform.position).normalized;
        transform.rotation = Quaternion.FromToRotation(Vector3.up, vec);
        GetComponent<Rigidbody2D>().velocity = vec * bulletSpeed;
    }
}
