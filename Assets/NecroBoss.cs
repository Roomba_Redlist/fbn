﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecroBoss : MonoBehaviour
{
    private bool isRunning;
    private GameObject player;
    private GameObject bullet1;
    private GameObject bullet2;
    private GameObject bullet3;
    private float dis;
    private Vector2 vec;
    private int dir;
    private float speed = 2;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
        bullet1 = (GameObject)Resources.Load("NecroBullet1");
        bullet2 = (GameObject)Resources.Load("NecroBullet2");
        bullet3 = (GameObject)Resources.Load("NecroBullet3");
        Debug.Log(bullet1.tag);
        StartCoroutine("Action");

    }

    // Update is called once per frame
    void Update()
    {
        dis = Vector3.Distance(transform.position, player.transform.position);
        vec = (transform.position - player.transform.position).normalized;
        if (vec.x > 0)
        {
            dir = -1;
        }
        else if (vec.x < 0)
        {
            dir = 1;
        }
    }
    IEnumerator Action()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            yield return HomingShot();
            yield return new WaitForSeconds(3f);
            yield return AimingShot();
            yield return new WaitForSeconds(3f);
            yield return MagicRain(8,5f);
            yield return new WaitForSeconds(4f);
            yield return AimingShot();
            yield return new WaitForSeconds(3f);
            yield return HomingShot();
            yield return new WaitForSeconds(3f);
            yield return MagicRain(8, -5f);
        }

    }
    IEnumerator Move(int count)
    {
        while (true)
        {
            Vector3 scale = transform.localScale;
            if (dir > 0)
            {
                scale.x = -1;
            }
            else if (dir < 0)
            {
                scale.x = 1;
            }
            transform.localScale = scale;
            rb.velocity = new Vector2(speed * dir, 0);
            yield return new WaitForSeconds(0.3f);
            count--;
            if (dis <= 6 || count==0)
            {
                Debug.Log("move end");
                yield break;
            }
        }
    }
    IEnumerator AimingShot()
    {
        Debug.Log("aimingshot start");
        yield return new WaitForSeconds(0.15f);
        GameObject shot = Instantiate(bullet1, transform.GetChild(0).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot2 = Instantiate(bullet1, transform.GetChild(1).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot3 = Instantiate(bullet1, transform.GetChild(2).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot4 = Instantiate(bullet1, transform.GetChild(3).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot5 = Instantiate(bullet1, transform.GetChild(4).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot6 = Instantiate(bullet1, transform.GetChild(5).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot7 = Instantiate(bullet1, transform.GetChild(6).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(0.15f);
        GameObject shot8 = Instantiate(bullet1, transform.GetChild(7).gameObject.transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(2f);
        Debug.Log("aimingshot end");
    }
    IEnumerator HomingShot()
    {
        GameObject homing = Instantiate(bullet2, transform.position, Quaternion.identity, transform);
        yield return new WaitForSeconds(2f);
    }
    IEnumerator MagicRain(int i,float vel)
    {
        rb.velocity = new Vector2(vel, 0);
        while (i >= 0)
        {

            GameObject rain = Instantiate(bullet3, transform.position, Quaternion.identity, transform);
            yield return new WaitForSeconds(0.6f);
            i--;
        }
        i = 8;
        rb.velocity = new Vector2(0, 0);
    }
}
