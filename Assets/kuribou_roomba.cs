﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kuribou_roomba : MonoBehaviour
{
    private float dis;
    public float speed=10f;
    public GameObject objA;
    public GameObject objB;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Apos = objA.transform.position;
        Vector3 Bpos = objB.transform.position;
        dis = Vector3.Distance(Apos, Bpos);

        if (dis <= 5)
        {
            rb.velocity = new Vector2(10, 0);
        }
        else
        {
            rb.velocity = new Vector2(speed, 0);
        }

    }
}
