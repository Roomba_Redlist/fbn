﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static int EquipedWeapon=0;
    public float runSpeed = 10.0f;
    public float jumpPower = 30.0f;
    public static float playerLife = 100f;
    public int playerDirection=1;
    public bool invincible = false;
    public Sprite[] playerSprite;
    public Rigidbody2D rb;
    private bool inputJump;
    private bool inputJumpUp;
    private bool isRunning;
    private float inputRun;
    private float jumpTime;
    private bool isJumping=false;
    private bool isGround=true;
    private float invincibleTime;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            EquipedWeapon++;
            if (EquipedWeapon == 3)
            {
                EquipedWeapon = 0;
            }
        }

        switch (EquipedWeapon)
        {
            case 0:
                transform.GetChild(0).gameObject.SetActive(true);
                transform.GetChild(1).gameObject.SetActive(true);
                transform.GetChild(2).gameObject.SetActive(false);
                transform.GetChild(3).gameObject.SetActive(false);
                break;
            case 1:
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(false);
                transform.GetChild(2).gameObject.SetActive(true);
                transform.GetChild(3).gameObject.SetActive(false);
                break;
            case 2:
                transform.GetChild(0).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(false);
                transform.GetChild(2).gameObject.SetActive(false);
                transform.GetChild(3).gameObject.SetActive(true);
                break;
        }
        inputRun = Input.GetAxis("Horizontal");
        Vector3 scale = transform.localScale;
        if (inputRun > 0)
        {
            inputRun = 1;
            playerDirection = 1;

        }
        else if(inputRun < 0)
        {
            inputRun = -1;
            playerDirection = -1;

        }

        if (playerDirection>0)
        {
            scale.x = 1;
        }else if (playerDirection < 0)
        {
            scale.x = -1;
        }
        transform.localScale = scale;
        if (inputRun == 0)
        {
            switch (EquipedWeapon)
            {
                case 0:
                    GetComponent<SpriteRenderer>().sprite = playerSprite[5];
                    break;
                case 1:
                    GetComponent<SpriteRenderer>().sprite = playerSprite[10];
                    break;
                case 2:
                    GetComponent<SpriteRenderer>().sprite = playerSprite[15];
                    break;
            }
        }
        else
        {
            switch (EquipedWeapon)
            {
                case 0:
                    StartCoroutine(PlayerAnim(6));
                    break;
                case 1:
                    StartCoroutine(PlayerAnim(11));
                    break;
                case 2:
                    StartCoroutine(PlayerAnim(16));
                    break;
            }
        }
        inputJump = Input.GetButton("Jump");
        inputJumpUp = Input.GetButtonUp("Jump");

        if (isGround == true)
        {
            isJumping = false;
        }

        if (invincibleTime >= 0)
        {
            invincibleTime -= Time.deltaTime;
            invincible = true;
        }
        else
        {
            invincible = false;
        }
        if (playerLife <= 0)
        {
            SceneManager.LoadScene("Result");
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            isGround = true;
            jumpTime = 0;
        }
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(inputRun * runSpeed, rb.velocity.y);
        if (inputJump && jumpTime < 0.2f && !isJumping)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
            jumpTime += Time.deltaTime;
            isGround = false;
        }
        if (inputJumpUp==true)
        {
            isJumping = true;
        }
        if (invincible)
        {
            float level = Mathf.Abs(Mathf.Sin(Time.time * 20));
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, level);
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        }
    }
    public void Damaged(float damage)
    {

        playerLife -= damage;
        invincibleTime = 1f;
    }
    IEnumerator PlayerAnim(int i)
    {

        if (isRunning)
        {
            yield break;
        }
        isRunning = true;

        GetComponent<SpriteRenderer>().sprite = playerSprite[i];
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().sprite = playerSprite[i+1];
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().sprite = playerSprite[i+2];
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().sprite = playerSprite[i+3];
        yield return new WaitForSeconds(0.1f);
        isRunning = false;
    }
}
