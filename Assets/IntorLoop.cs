﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
    public AudioClip clip;
    private SoundManager _mng;

    void Start()
    {
        _mng = gameObject.AddComponent<SoundManager>();
        BgmDefine def = new BgmDefine();
        def.endTime = 3240f;
        def.startTime = 0;
        def.loopTime = 360f;
        def.key = "testBgm";
        _mng.RegisterBgm(def, clip);
    }

    void OnGUI()
    {
        if (GUILayout.Button("Play BGM"))
        {
            _mng.PlayBgm("testBgm");
        }
    }
}
