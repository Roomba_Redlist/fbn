﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public float enemyLife;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (enemyLife <= 0)
        {
            Destroy(gameObject);
        }
    }
    public void Damaged(float damage)
    {
        enemyLife -= damage;
        Debug.Log("enemylife = "+enemyLife);
    }
}
