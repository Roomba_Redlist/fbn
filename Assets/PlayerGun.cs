﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGun : MonoBehaviour
{
    public GameObject bullet;
    public int dir;
    private float coolTime = 0;
    // Update is called once per frame
    void Update()
    {
        dir = gameObject.GetComponentInParent<PlayerController>().playerDirection;
        if (Input.GetButton("Fire1") && coolTime == 0)
        {
            GameObject shot = Instantiate(bullet, transform.position, Quaternion.identity, transform);
            coolTime = 0.1f;
        }
        if (coolTime >= 0)
        {
            coolTime -= Time.deltaTime;
        }else if (coolTime <= 0)
        {
            coolTime = 0;
        }
    }
}
