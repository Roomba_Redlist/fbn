﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostEnemy : MonoBehaviour
{
    private Vector2 vec;
    private bool isRunning;
    private GameObject player;
    public GameObject bullet;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        player=GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        vec = (transform.position - player.transform.position).normalized;
        StartCoroutine("Shot");

        Vector3 scale = transform.localScale;
        if (vec.x > 0)
        {
            scale.x = 1;
        }
        else if (vec.x < 0)
        {
            scale.x = -1;
        }
        transform.localScale = scale;
    }
    IEnumerator Shot()
    {
        if (isRunning)
        {
            yield break;
        }
        isRunning = true;
        GameObject shot = Instantiate(bullet, transform.position, Quaternion.FromToRotation(Vector3.up, vec));
        shot.GetComponent<Rigidbody2D>().velocity = vec * bulletSpeed;
        yield return new WaitForSeconds(2);
        isRunning = false;
    }
}
