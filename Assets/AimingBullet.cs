﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingBullet : MonoBehaviour
{
    public float timeRag = 1.5f;
    public float bulletSpeed;
    private Vector2 vec;
    private GameObject player;
    private bool shot;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");


    }

    // Update is called once per frame
    void Update()
    {
        timeRag -= Time.deltaTime;
        if (timeRag <= 0 && shot==false)
        {
            vec = (transform.position - player.transform.position).normalized;
            GetComponent<Rigidbody2D>().velocity = vec * bulletSpeed;
            shot = true;
        }
    }
}
