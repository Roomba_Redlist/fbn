﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // ←※これを忘れずに入れる

public class HpBarCtrl : MonoBehaviour
{

    Slider hpslider;
    private float maxHp=100f;
    private float nowHp=100f;
    void Start()
    {
        // スライダーを取得する
        hpslider = GameObject.Find("HpSlider").GetComponent<Slider>();
        //スライダーの最大値
        hpslider.maxValue = maxHp;
        hpslider.value = nowHp;




    }

    
    void Update()
    {
      if(Input.GetMouseButton(0))
        {
            hpslider.value -= 10f;

        }
    }
}

