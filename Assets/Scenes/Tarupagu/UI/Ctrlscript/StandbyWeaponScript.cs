﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StandbyWeaponScript : MonoBehaviour
{
    public int subweapons;
    public int select;
    // Start is called before the first frame update
    void Start()
    {
        select = 1;
        subweapons = select;

        switch (subweapons)
        {
            case 0://剣アイコンを表示
                GameObject.Find("StandbySwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("StandbyGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("StandbyMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;

            case 1://銃アイコンを表示
                GameObject.Find("StandbyGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("StandbySwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("StandbyMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;

            case 2://魔法（杖）アイコンを表示
                GameObject.Find("StandbyMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("StandbySwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("StandbyGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;
            default:
                GameObject.Find("StandbySwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("StandbyGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("StandbyMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;
        }

    }

    private GameObject standbyWeapon;
    private RectTransform rect;
    int a = 0;
    // Update is called once per frame
    void Update()
    {
        standbyWeapon = GameObject.Find("StandbyWeapon");
        
        if (Input.GetKeyDown(KeyCode.C))
        {

          switch(a)
            {
                case 0:
                rect = GameObject.Find("StandbyWeapon").GetComponent<RectTransform>();

                standbyWeapon.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
                rect.localPosition += new Vector3(-100f, 65f, 0f);
                a=1;
                    break;


                case 1:
                rect = GameObject.Find("StandbyWeapon").GetComponent<RectTransform>();

                standbyWeapon.GetComponent<RectTransform>().localScale = new Vector3(0.8f, 0.8f, 0.8f);
                rect.localPosition -= new Vector3(-100f, 65f, 0f);
                a = 0;
                    break;
            }
        }
    }
}
