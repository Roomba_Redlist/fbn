﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EquipedWeaponScript : MonoBehaviour
{

    public int nowweapons;//仮の関数

    public int select;

    // Update is called once per frame
    void Start()
    {
        select = 2;
        nowweapons = select;


        switch (nowweapons)
        {
            case 0://剣アイコンを表示
                GameObject.Find("EquipedSwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("EquipedGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("EquipedMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;

            case 1://銃アイコンを表示
                GameObject.Find("EquipedGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("EquipedSwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("EquipedMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;

            case 2://魔法（杖）アイコンを表示
                GameObject.Find("EquipedMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                GameObject.Find("EquipedSwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("EquipedGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);

                break;
            default:
                GameObject.Find("EquipedSwordRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("EquipedGunRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                GameObject.Find("EquipedMagicRawImage").GetComponent<RawImage>().color = new Color(1.0f, 1.0f, 1.0f, 0f);
                break;
        }
    }


    private GameObject equipedWeapon;
    private RectTransform rect;
    int a = 0;


    // Update is called once per frame
    void Update()
    {
        equipedWeapon = GameObject.Find("EquipedWeapon");
        if (Input.GetKeyDown(KeyCode.C))
        {


            switch (a)
            {
                case 0:
                    rect = GameObject.Find("EquipedWeapon").GetComponent<RectTransform>();

                    equipedWeapon.GetComponent<RectTransform>().localScale = new Vector3(0.8f, 0.8f, 1f);
                    rect.localPosition += new Vector3(100f, -65f, 0f);
                    a = 1;
                    break;


                case 1:
                    rect = GameObject.Find("EquipedWeapon").GetComponent<RectTransform>();

                    equipedWeapon.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
                    rect.localPosition -= new Vector3(100f, -65f, 0f);
                    a = 0;
                    break;
            }


        }
    }
}
        

