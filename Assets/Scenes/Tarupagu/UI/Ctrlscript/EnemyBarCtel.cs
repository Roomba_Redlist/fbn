﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyBarCtel : MonoBehaviour
{
    Slider enemyslider;

    // Start is called before the first frame update
    void Start()
    {
        //スライダーを取得
        enemyslider = GameObject.Find("EnemySlider").GetComponent<Slider>();

        float maxhp = 100f;
        float nowhp = 100f;

        //敵のHP最大値
        enemyslider.maxValue = maxhp;

        //敵のHP現在値
        enemyslider.value = nowhp;
        
    }

    // Update is called once per frame
    void Update()
    {
        //クリックでHPを減らす
        if(Input.GetMouseButton(0))
        {
            enemyslider.value -= 5f;
        }
    }
}
